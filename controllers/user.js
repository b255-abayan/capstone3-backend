const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");

// User Login
module.exports.loginUser = async (reqBody) => {
  try {
    return await User.findOne({
      email: reqBody.email,
    }).then((result) => {
      // User does not exist
      if (result == null) {
        return { auth: "Authentication Failed" };
        // User exists
      } else {
        // Creates the variable "isPasswordCorrect" to return the result of the comparing login form password and the database password
        // The "compareSync" method is used to compare a non encrypted password from the login from to the encrypted password retrieved from the database
        const isPasswordCorrect = bcrypt.compareSync(
          reqBody.password,
          result.password
        );

        // If the passwords match/result of the above code is true
        if (isPasswordCorrect) {
          // Generate an access token
          // Uses the "createAccessToken" method defined in the "auth.js" file
          // Returning an object back to the frontend application is common practice to ensure information is properly labeled
          return { access: auth.createAccessToken(result) };
          // Passwords do not match
        } else {
          return { auth: "Authentication Failed" };
        }
      }
    });
  } catch (err) {
    return { auth: "Authentication Failed" };
  }
};

// User Registration
module.exports.registerUser = async (reqBody) => {
  try {
    return await User.findOne({ email: reqBody.email }).then((result) => {
      // User does not exist
      if (result == null) {
        let newUser = new User({
          firstName: reqBody.firstName,
          lastName: reqBody.lastName,
          email: reqBody.email,
          password: bcrypt.hashSync(reqBody.password, 10),
        });
        return newUser.save().then((user, error) => {
          if (!error) {
            return { message: "User created successfully" };
          }
        });

        // User exists
      } else {
        return { message: "Account already registered" };
      }
    });
  } catch (err) {
    return { message: false };
  }
};

// Get All User details
module.exports.getAllUserDetails = async () => {
  try {
    return await User.find({}).then((users) => {
      return users;
    });
  } catch (err) {
    return { message: err.message };
  }
};

// Get Authenticated User details
module.exports.getAuthenticatedUserDetails = async (userData) => {
  try {
    return await User.find({ email: userData.email }).then((users) => {
      return users;
    });
  } catch (err) {
    return { message: err.message };
  }
};

// Check user if exists
module.exports.isUserExists = async (reqBody) => {
  try {
    return await User.find({ email: reqBody.email }).then((users) => {
      if (users.length > 0) {
        return true;
      } else {
        return false;
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

// Get User Name
module.exports.getUserName = async (reqParams, reqBody) => {
  try {
    return await User.find({ _id: reqParams.id }).then((user) => {
      if (user.length > 0) {
        return { data: user };
      } else {
        return false;
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

//Grant Admin Access
module.exports.grantAdmin = async (reqBody) => {
  try {
    // return await User.find({ _id: reqBody.userId }).then((user) => {
    //   if (user.length > 0) {
    //     return { data: user };
    //   } else {
    //     return false;
    //   }
    // });
    let updatedUser = {
      isAdmin: true,
    };
    return User.findByIdAndUpdate(reqBody.userId, updatedUser).then(
      (user, error) => {
        if (error) {
          return { message: error.message };
        } else {
          return { message: "success" };
        }
      }
    );
  } catch (err) {
    return { message: err.message };
  }
};
