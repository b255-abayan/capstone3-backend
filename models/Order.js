const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "UserId is required"],
  },
  ownerName: {
    type: String,
  },
  products: [
    {
      productId: {
        type: String,
        required: [true, "ProductId is required"],
      },
      quantity: {
        type: Number,
        required: [true, "Quantity is required"],
      },
      productName: {
        type: String,
      },
    },
  ],
  totalAmount: {
    type: Number,
    default: 0,
  },
  purchasedOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Order", orderSchema);
