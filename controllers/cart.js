const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
// const Order = require("../models/Order");
const Product = require("../models/Product");
const Cart = require("../models/Cart");

// Add to cart (Authenticated User)
module.exports.addToCart = async (reqBody, userData) => {
  /*
    If product is not found on the database, throw message "Product you are looking for is not found"
    Else, save product 
    */
  try {
    return await Product.findOne({ _id: reqBody.productId }).then((product) => {
      //Check if product is found on the database

      if (product) {
        let productFound = product.name;
        /*
                If product found is not active, throw message "You cannot place this product. Product is not available as of the moment"
                Else, save product 
                */
        if (product.isActive == true) {
          let newOrder = new Cart({
            userId: userData.id,
            itemPrice: product.price,
            addedOn: new Date(),
          });
          newOrder.products.push({
            productId: reqBody.productId,
            productName: productFound,
            quantity: reqBody.quantity,
          });
          newOrder.totalAmount = product.price * reqBody.quantity;
          return newOrder.save().then((product, error) => {
            if (error) {
              return { message: error.message };
            } else {
              return {
                message: "success",
              };
            }
          });
        } else {
          return {
            message: "notavailable",
          };
        }
      } else {
        return {
          message: "notfound",
        };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

// Update cart item quantity (Authenticated User)
module.exports.updateQuantity = async (reqParams, reqBody, userData) => {
  /*
    If product is not found on the database, throw message "Product you are looking for is not found"
    Else, save product 
    */
  try {
    return await Cart.findOne({ _id: reqParams.id }).then((cart) => {
      //Check if product is found on the database
      if (cart) {
        if (cart.userId == userData.id) {
          let updatedCart = {
            products: [],
          };
          updatedCart.totalAmount = cart.itemPrice * reqBody.quantity;
          updatedCart.products.push({ quantity: reqBody.quantity });

          return Cart.findByIdAndUpdate(reqParams.id, updatedCart).then(
            (cart, error) => {
              if (err) {
                return { message: error.message };
              } else {
                return { message: "Cart updated successfully" };
              }
            }
          );
        } else {
          return {
            message: "You cannot make a changes to a cart you do not own",
          };
        }
      } else {
        return {
          message:
            "Sorry, the cart item you are trying to update is not found on our record",
        };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

// Delete cart item (Authenticated User)
module.exports.deleteCartItem = async (reqParams, reqBody, userData) => {
  /*
    If product is not found on the database, throw message "Product you are looking for is not found"
    Else, save product 
    */
  try {
    return await Cart.findOne({ _id: reqParams.id }).then((cart) => {
      //Check if product is found on the database
      if (cart) {
        if (cart.userId == userData.id) {
          return Cart.findByIdAndDelete(reqParams.id).then((cart, error) => {
            if (error) {
              return { message: error.message };
            } else {
              return { message: "success" };
            }
          });
        } else {
          return {
            message: "cartnotown",
          };
        }
      } else {
        return {
          message: "notfound",
        };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

// Getting all cart item subtotal
module.exports.getCartSubTotal = (userData) => {
  try {
    return Cart.find(
      { userId: userData.id },
      { userId: 1, totalAmount: 1 }
    ).then((carts) => {
      if (carts.length > 0) {
        return carts;
      } else {
        return { message: "No cart item(s) found on our record" };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

// Getting cart total
module.exports.getCartTotal = (userData) => {
  try {
    let myCartTotal = Cart.aggregate([
      { $match: { userId: userData.id } },
      { $group: { _id: "$userId", TotalAmount: { $sum: "$totalAmount" } } },
    ]);
    return myCartTotal;
  } catch (err) {
    return { message: err.message };
  }
};

// Getting my cart
module.exports.getMyCartCount = (userData) => {
  try {
    let myCartTotal = Cart.aggregate([
      { $match: { userId: userData.id } },
      { $group: { _id: "$userId", myCartCount: { $sum: 1 } } },
    ]);
    return myCartTotal;
  } catch (err) {
    return { message: err.message };
  }
};

// Getting my cart items
module.exports.getMyCart = async (userData) => {
  try {
    // let myCartTotal = Cart.aggregate([
    //   { $match: { userId: userData.id } },
    //   { $group: { _id: "$userId", myCartCount: { $sum: 1 } } },
    // ]);
    // return myCartTotal;
    return await Cart.find({ userId: userData.id }).then((carts) => {
      /*
				If the product you are trying to retrieve is found on the record throw a message "Sorry, the product you are trying to retrieve is not found on our record".
				Else, return product
			*/
      if (carts) {
        return carts;
      } else {
        return {
          message:
            "Sorry, the product you are trying to retrieve is not found on our record",
        };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};
