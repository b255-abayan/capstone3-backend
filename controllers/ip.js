const IP = require("../models/IP");

// User Registration
module.exports.getIpAddress = async (reqBody) => {
  try {
    let newIP = new IP({
      ipAddress: reqBody.ipAddress,
    });
    return newIP.save().then((ip, error) => {
      if (!error) {
        console.log(`Site has been accessed thru ${reqBody.ipAddress}`);
        return { message: "success" };
      }
    });
  } catch (err) {
    return { message: false };
  }
};
