const express = require("express");
const router = express.Router();
const auth = require("../auth");
const cartController = require("../controllers/cart");

// Add to cart (Authenticated User)
router.post("/", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    cartController
      .addToCart(req.body, userData)
      .then((resultFromController) => res.send(resultFromController));
  } catch (err) {
    // return { message: err.message };
  }
});

// Change Cart placed order quantity (Authenticated User)
router.patch("/:id/update-quantity", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    cartController
      .updateQuantity(req.params, req.body, userData)
      .then((resultFromController) => res.send(resultFromController));
  } catch (err) {
    return { message: err.message };
  }
});

// Remove cart item (Authenticated User)
router.delete("/:id/delete", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    cartController
      .deleteCartItem(req.params, req.body, userData)
      .then((resultFromController) => res.send(resultFromController));
  } catch (err) {
    return { message: err.message };
  }
});

// Retrieve subtotal in cart item (Authenticated User)
router.get("/cart-subtotal", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    cartController
      .getCartSubTotal(userData)
      .then((resultFromController) => res.send(resultFromController));
  } catch (err) {
    return { message: err.message };
  }
});

// Retrieve total in cart (Authenticated User)
router.get("/cart-total", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    cartController
      .getCartTotal(userData)
      .then((resultFromController) => res.send(resultFromController));
  } catch (err) {
    return { message: err.message };
  }
});

// Retrieve total count of cart (Authenticated User)
router.get("/mycart", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    cartController
      .getMyCartCount(userData)
      .then((resultFromController) => res.send(resultFromController));
  } catch (err) {
    return { message: err.message };
  }
});

// Retrieve cart (Authenticated User)
router.get("/mycart-items", auth.verify, (req, res) => {
  try {
    const userData = auth.decode(req.headers.authorization);
    cartController
      .getMyCart(userData)
      .then((resultFromController) => res.send(resultFromController));
  } catch (err) {
    return { message: err.message };
  }
});

module.exports = router;
