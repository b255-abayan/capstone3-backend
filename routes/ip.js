const express = require("express");
const router = express.Router();
const ipController = require("../controllers/ip");

router.post("/", (req, res) => {
  ipController
    .getIpAddress(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
