const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/user");

// Route for user registration
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Route for getting users details
router.get("/", auth.verify, (req, res) => {
  userController
    .getAllUserDetails()
    .then((resultFromController) => res.send(resultFromController));
});

// Route for getting authenticated users details
router.get("/loggedin", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .getAuthenticatedUserDetails(userData)
    .then((resultFromController) => res.send(resultFromController));
});

// Route to check if user exists
router.post("/checkEmail", (req, res) => {
  userController
    .isUserExists(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Route to get Username
router.get("/:id/name", (req, res) => {
  userController
    .getUserName(req.params, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Route for granting admin access
router.post("/grant-admin", auth.verify, (req, res) => {
  userController
    .grantAdmin(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
