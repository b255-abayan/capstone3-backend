const bcrypt = require("bcrypt");
const auth = require("../auth");
const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");

module.exports.createOrder = async (reqBody, userData) => {
  try {
    return await Product.findOne({ _id: reqBody.productId }).then((product) => {
      //Check if product is found on the database

      if (product) {
        let productFound = product.name;
        /*
                If product found is not active, throw message "You cannot place this product. Product is not available as of the moment"
                Else, save product 
                */
        if (product.isActive == true) {
          let newOrder = new Order({
            userId: reqBody.userId,
            ownerName: userData.firstName,
            purchasedOn: new Date(),
          });
          newOrder.products.push({
            productId: reqBody.productId,
            productName: productFound,
            quantity: reqBody.quantity,
          });
          newOrder.totalAmount = product.price * reqBody.quantity;
          return newOrder.save().then((product, error) => {
            if (error) {
              return { message: "Something wen't wrong" };
            } else {
              return {
                message: "success",
              };
            }
          });
        } else {
          return {
            message: "notavailable",
          };
        }
      } else {
        return {
          message: "notfound",
        };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

// Get authenticated user orders
module.exports.getMyOrders = async (authenticatedUser) => {
  try {
    return Order.find({ userId: authenticatedUser.id }).then((orders) => {
      if (orders.length > 0) {
        return orders;
      } else {
        return { message: "No order(s) found on our record" };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

// Get all orders (Admin Only)
module.exports.getAllOrders = async () => {
  try {
    return Order.find({}).then((orders) => {
      if (orders.length > 0) {
        return orders;
      } else {
        return { message: "No order(s) found on our record" };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};
