const Product = require("../models/Product");

// Creating Product
module.exports.createProduct = (reqBody) => {
  try {
    return Product.findOne({ name: reqBody.name }).then((result) => {
      /*
				If the product you are trying to create is found on the record throw a message "Product you are trying to create is already exists".
				If not, allow the code execution 
			*/
      if (result == null) {
        let newProduct = new Product({
          name: reqBody.name,
          description: reqBody.description,
          price: reqBody.price,
        });
        /*
                    If the product creation is success throw a message "Product created successfully".
                    If not, throw error message
                */
        return newProduct.save().then((product, error) => {
          if (error) {
            return { message: error.message };
          } else {
            return { message: "success" };
          }
        });

        // If product exists
      } else {
        return {
          message: "exists",
        };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

// Getting all products
module.exports.getAllProducts = () => {
  try {
    return Product.find({}).then((products) => {
      /*
                If no product(s) found throw a message "No product(s) found on our record".
                Else, return product(s)
            */
      if (products !== null) {
        return products;
      } else {
        return { message: "No product(s) found on our record" };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

// Getting all active products
module.exports.getAllActiveProducts = () => {
  try {
    return Product.find({ isActive: true }).then((products) => {
      /*
                If no active product(s) found throw a message "No active product(s) found on our record".
                Else, return product(s)
            */
      if (products.length > 0) {
        return products;
      } else {
        return { message: "No active product(s) found on our record" };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

//Getting a specific product
module.exports.getProduct = async (reqParams) => {
  try {
    return await Product.findById(reqParams.productId).then((product) => {
      /*
				If the product you are trying to retrieve is found on the record throw a message "Sorry, the product you are trying to retrieve is not found on our record".
				Else, return product
			*/
      if (product) {
        return product;
      } else {
        return {
          message:
            "Sorry, the product you are trying to retrieve is not found on our record",
        };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

//Updating a specific product
module.exports.updateProduct = async (reqParams, reqBody) => {
  try {
    let updatedProduct = {
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
      isActive: reqBody.isActive,
    };
    return await Product.findByIdAndUpdate(
      reqParams.productId,
      updatedProduct
    ).then((product, error) => {
      /*
				Check if the product trying to update is found on the records.
				If not, do not allow the execution and throw a message "Sorry, the product you are trying to update is not found on our record"
			*/
      if (product) {
        if (error) {
          return { message: error.message };
        } else {
          return { message: "success" };
        }
      } else {
        return {
          message: "notfound",
        };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};

//Archieving product
module.exports.archieveProduct = async (reqParams) => {
  try {
    let updatedProduct = {
      isActive: false,
    };
    return await Product.findByIdAndUpdate(
      reqParams.productId,
      updatedProduct
    ).then((product, error) => {
      /*
				Check if the product trying to update is found on the records.
				If not, do not allow the execution and throw a message "Sorry, the product you are trying to archieve is not found on our record"
			*/
      if (product) {
        if (product.isActive == true) {
          if (error) {
            return { message: error.message };
          } else {
            return { message: "success" };
          }
        } else {
          return { message: "alreadyArchived" };
        }
      } else {
        return {
          message: "notfound",
        };
      }
    });
  } catch (err) {
    return { message: "error" };
  }
};

//Activating product
module.exports.activateProduct = async (reqParams) => {
  try {
    let updatedProduct = {
      isActive: true,
    };
    return await Product.findByIdAndUpdate(
      reqParams.productId,
      updatedProduct
    ).then((product, error) => {
      /*
				Check if the product trying to update is found on the records.
				If not, do not allow the execution and throw a message "Sorry, the product you are trying to archieve is not found on our record"
			*/
      if (product) {
        if (product.isActive == false) {
          if (error) {
            return { message: error.message };
          } else {
            return { message: "success" };
          }
        } else {
          return { message: "alreadyActive" };
        }
      } else {
        return {
          message: "notfound",
        };
      }
    });
  } catch (err) {
    return { message: "error" };
  }
};

//Getting a product name
module.exports.getProductName = async (reqParams) => {
  try {
    return await Product.findById(reqParams.productId).then((product) => {
      /*
				If the product you are trying to retrieve is found on the record throw a message "Sorry, the product you are trying to retrieve is not found on our record".
				Else, return product
			*/
      if (product) {
        return { message: product };
      } else {
        return {
          message:
            "Sorry, the product you are trying to retrieve is not found on our record",
        };
      }
    });
  } catch (err) {
    return { message: err.message };
  }
};
