const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
    // required: [true, "Course is required"]
  },
  products: [
    {
      productId: {
        type: String,
        required: [true, "ProductId is required"],
      },
      quantity: {
        type: Number,
        required: [true, "Quantity is required"],
      },
      productName: {
        type: String,
      },
    },
  ],
  itemPrice: {
    type: Number,
    default: 0,
  },

  totalAmount: {
    type: Number,
    default: 0,
  },
  addedOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Cart", cartSchema);
