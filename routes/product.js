const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/product");

// Route for creating product(Admin Only)
router.post("/", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  /*
		Check if the user authenticated is Administrator.
		If not, do not allow the execution and throw a message "Only Administrator can create a product"
	*/
  if (userData.isAdmin == true) {
    productController
      .createProduct(req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "Only Administrator can create a product" });
  }
});

// Route for getting all products
router.get("/all", (req, res) => {
  productController
    .getAllProducts(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Route for getting all active products
router.get("/", (req, res) => {
  productController
    .getAllActiveProducts(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Route for retrieving a specific product
router.get("/:productId", (req, res) => {
  productController
    .getProduct(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

//Route for updating a specific product
router.put("/:productId", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin == true) {
    productController
      .updateProduct(req.params, req.body)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "Only Administrator can update product" });
  }
});

//Route for archieving product
router.put("/:productId/archieve", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin == true) {
    productController
      .archieveProduct(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "Only Administrator can archieve product" });
  }
});

//Route for activating product
router.put("/:productId/activate", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin == true) {
    productController
      .activateProduct(req.params)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "Only Administrator can archieve product" });
  }
});

//Route for getting product name
router.get("/:productId/name", (req, res) => {
  productController
    .getProductName(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
