const mongoose = require("mongoose");

const iPSchema = new mongoose.Schema({
  
  ipAddress: {
    type: String,
  },
  accessedOn: {
    type: Date,
    default: new Date()
  },
});

module.exports = mongoose.model("IP", iPSchema);
