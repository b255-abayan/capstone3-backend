const express = require("express");
const router = express.Router();
const auth = require("../auth");
const orderController = require("../controllers/order");

// Create Order
router.post("/", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);
  if (userData.isAdmin == false) {
    orderController
      .createOrder(req.body, userData)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "Only Non-admin can place an order" });
  }
});

// Retrieve authenticated user orders
router.get("/mycart", (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  orderController
    .getMyOrders(userData)
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve all orders (Admin Only)
router.get("/all", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin == true) {
    orderController
      .getAllOrders(req.body, userData)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ message: "Only Administrator can retrive all orders" });
  }
});

module.exports = router;
